# Project Name:  Lesson 3 Version Control


## Course Title:
Web Application Development

## Assignment Date:  
September 28, 2018

## Student Name:  
Christopher Ehster

## Project Description:
This lesson demonstrated the basic use and implementation of version control systems in application development,
with an emphasis on the use of Git and GitHub technologies.


## Lessons Learned in the Assignment:
1. We learned how to create, and link, our GitHub repositories.
2. We learned how to initialize our local repositories in Cloud 9.
3. We learned how to commit our files with Git, and push them to a remote repository on GitHub.

