## Project Name:  Expense Calculator Application

### Course Title:
Web Application Development

### Assignment Date:  
10/18/2018

### Student Name:  
Christopher Ehster

### Project Description:
An expense calculator that breaks down different categories of expenses. Accepts user input and calculates totals/percentages. 


### Lessons Learned in the Assignment:
1. You can select nodes on an HTML page by using the getElementById keyword.
2. Input recieved from the user are typically stored as string data types and have to be converted to other usuable types with the parse function.
3. Functions are blocks of code used in JavaScript to allow the reuse of short segments of useful code. In the case of this assignment we used a calculation function to allow the web page to perform calculations on various user inputs.



