## Project Name:  Currency Exchange Calculator Application

### Course Title:
Web Application Development

### Assignment Date:  
11/02/2018

### Student Name:  
Christopher Ehster

### Project Description:
In this assignment we calculated a currency exchange with a conditional statement.


### Lessons Learned in the Assignment:
1. Initializing variables and setting them to specific data values.
2. Using multi-way conditional statements to sort through possible paths the program might take.
3. Selected nodes to manipulate specific parts of the webpage. 

