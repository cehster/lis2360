## Project Name:  Photo Gallery Application

### Course Title:
Web Application Development

### Assignment Date:  
11/20/2018

### Student Name:  
Christopher Ehster

### Project Description:
Create an interactive photo gallery that allows you to preview additional images.


### Lessons Learned in the Assignment:
1. This assignment demonstrated navigation of elements on the DOM.
2. We created loops to interact with elements in an array.
3. We created our own nodes and modified the pages behavior to make a DHTML document.

