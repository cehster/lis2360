## Project Name:  Recipe Display Application

### Course Title:
Web Application Development

### Assignment Date:  
12/4/2018

### Student Name:  
Christopher Ehster

### Project Description:
We made a recipe application dynamic by using jQuery to animate behavior.


### Lessons Learned in the Assignment:
1. Properly used the jQuery syntax.
2. Utilized jQuery functions that allowed us to traverse the DOM.
3. Used jQuery functions to animate features on the page.

