## Project Name:  Seminole Movie Connection Application using TMDB API

### Course Title:
LIS 2360:  Web Application Development

### Assignment Date:  
12/10/18

### Student Name:  
Christopher Ehster

### Project Description:
We created a webpage that interacts with a movie database api.


### Lessons Learned in the Assignment:
1. Learned how to register for an API key.
2. Learned how HTTPS Request and Response works.
3. Used JSON to query an open-source database.
4. Learned how to manipulate the DOM with AJAX requests.
5. Learned how to read API documentation.