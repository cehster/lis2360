# LIS2360 - Web Application Development

## Chris Ehster

### LIS2360 Requirements:

*Course Work Links:*

1. [L3 README.md](L3/README.md "My L3 README.md file")
    - Create and Link GitHub repositories with Git.
      - Later ported to BitBucket to consolidate school projects.
    - Initialize repositories within Cloud 9 (an online IDE).
    - Commited a static website files with Git and pushed them to a repo on GitHub (now Bitbucket).

2. [L4 README.md](L4/README.md "My L4 README.md file")
   - Created an expense calculator that accepts input and displays percentages and totals.
     - Selected nodes on an HTML page with vanilla JavaScript.
     - Stored user input in `string` variables and used parsing functions to manipulate the data.
     - Created custom functions in JavaScript.

3. [L5 README.md](L5/README.md "My L5 README.md file")
   - Create a currency exchange calculator.
     - Demonstrate use of conditional statements in JavaScript to create control flow.

4. [L6 README.md](L6/README.md "My L6 README.md file")
    - Create an interactive photo gallery that allows users to dynamically affect the webpage.
      - Navigate elements on the DOM with vanilla JavaScript.
      - Loop through DOM elements as array data type.
      - Create nodes and modify pages to create a DHTML page.

5. [L7 README.md](L7/README.md "My L7 README.md file")
   - Create dynamic recipe application with jQuery.
     - Import jQuery modules and practice jQuery syntax to target elements of the DOM.
     - Use jQuery functions to animate features on the page.

6. [L8 README.md](L8/README.md "My L8 README.md file")
    - Create a webpage that interacts with IMDB's web API to create a searchable database of movies.
      - Registered for an API key.
      - Used AJAX with JSON to access data requested from IMDB.